import asyncio
import math
import logging
import shlex
import sys
import traceback
from logging.handlers import RotatingFileHandler

LOG_FORMAT = "{%(asctime)s} [%(name)s:%(levelname)s]: %(message)s"
LOG_SIZE = 1024 * 1024

SERVER_HOSTNAME = "localhost"
SERVER_PORT = 8888

DEFAULT_ENCODING = "utf-8"

rotating_handler = RotatingFileHandler("engine.log", maxBytes=LOG_SIZE)
stream_handler = logging.StreamHandler()

# noinspection PyArgumentList
logging.basicConfig(
    level=logging.DEBUG,
    format=LOG_FORMAT,
    handlers=(
        stream_handler,
        rotating_handler
    )
)

descriptors = {}

users = {
    "nattfarinn": "duparzepa",
}


class Character:
    def __init__(self, name, archetype, level, hp):
        self.name = name
        self.archetype = archetype
        self.level = level
        self.hp = hp
        self.current_hp = self.hp

class Enemy(Character):
    pass


characters = {
    "nattfarinn": [
        Character("Nattfarinn", "Warrior", 15, 300),
        Character("Uchtenhagen", "Mage", 31, 180),
    ]
}


class GameState:
    def __init__(self, descriptor):
        self.descriptor = descriptor

    def initialize(self):
        pass

    def get_command(self, input_message):
        parts = shlex.split(input_message)
        return parts[0], parts[1:]

    async def parse_input(self):
        pass


class CharacterSelect(GameState):
    def __init__(self, descriptor, authentication):
        super().__init__(descriptor)
        self.character = None
        self.authentication = authentication

    def initialize(self):
        self.character = None

    async def parse_input(self):
        if not self.authentication.authenticated:
            self.descriptor.set_game_state(Authentication)
        else:
            if not self.character:
                self.descriptor.send("Zalogowano jako: %s" %
                                     self.authentication.username)
                self.descriptor.send("Wybierz postać (numer):")
                for index, character in enumerate(
                        characters[self.authentication.username]):
                    self.descriptor.send("%d.  %s [%s, %d level]" % (
                        index + 1,
                        character.name,
                        character.archetype,
                        character.level
                    ))
                choice = await self.descriptor.read()
                try:
                    index = int(choice) - 1
                    self.descriptor.character = characters[
                        self.authentication.username][index]
                    self.descriptor.set_game_state(World)
                except KeyError:
                    pass


class Authentication(GameState):
    def __init__(self, descriptor):
        super().__init__(descriptor)
        self.username = None
        self.authenticated = False

        self.initialize()

    def initialize(self):
        self.username = None
        self.authenticated = False

    async def parse_input(self):
        if not self.authenticated:
            self.descriptor.send("Nazwa użytkownika:")
            username = await self.descriptor.read()
            if username in users:
                self.descriptor.send("Hasło:")
                password = await self.descriptor.read()
                if password == users[username]:
                    self.username = username
                    self.authenticated = True
                    self.descriptor.set_game_state(CharacterSelect, self)
                else:
                    self.descriptor.set_game_state(Authentication)
        else:
            self.descriptor.set_game_state(CharacterSelect)


class World(GameState):
    def __init__(self, descriptor):
        super().__init__(descriptor)

    def send_prompt(self):
        self.descriptor.send("\n%d/%dhp >" % (
            self.descriptor.character.current_hp,
            self.descriptor.character.hp
        ))

    def initialize(self):
        self.descriptor.send("\nŚcieżka w lesie\n")
        self.descriptor.send("Wąska wydeptana ścieżka przecina tędy gęsty "
                             "las i znika w oddali zasłonięta drzewami. "
                             "Gdyby nie absolutny brak żadnych "
                             "dźwięków, mogło by tutaj być nawet "
                             "przytulnie.\n")
        self.descriptor.send("Bandyta przechadza się niedaleko, "
                             "ewidentnie kombinując coś nikczemnego.\n\n")

    async def parse_input(self):
        self.send_prompt()

        command_input = await self.descriptor.read()
        (command, arguments) = self.get_command(command_input)

        if command == "quit":
            self.descriptor.send("Opuszczasz tę krainę...")
            await self.descriptor.disconnect()
        elif command == "heal":
            self.descriptor.character.current_hp = \
                self.descriptor.character.hp
        elif command == "look":
            self.initialize()
        elif command == "kill":
            if arguments[0] == "bandyta":
                self.descriptor.send("Ruszasz do ataku...")
                self.descriptor.set_game_state(
                    InCombat,
                    [Enemy("Bandyta", "Złodziej", 25, 220)]
                )
            else:
                self.descriptor.send("Nie widzisz tutaj nikogo takiego.")


class InCombat(World):
    def __init__(self, descriptor, enemies):
        super().__init__(descriptor)

        self.enemies = enemies

    def get_bar(self, subject, enemy=False):
        percent = (subject.current_hp / subject.hp) * 100
        full = math.floor(percent / 10)
        half = int((percent % 10) > 5)

        bar = "".rjust(full, "=").rjust(full + half, "-").rjust(10, " ")

        if enemy:
            return "[%s] %s" % (bar[::-1], subject.name)
        return "%s [%s]" % (subject.name, bar)

    async def parse_input(self):
        self.descriptor.send("%s vs. %s\n" % (
            self.get_bar(self.descriptor.character),
            self.get_bar(self.enemies[0], True)
        ))
        self.send_prompt()
        action_input = await self.descriptor.read()
        (command, arguments) = self.get_command(action_input)

        if command == "flee":
            self.descriptor.send("Nie jesteś z tego dumny, "
                                 "ale przynajmniej żyjesz.\n")
            self.descriptor.set_game_state(World)
        elif command == "wound":
            if arguments[0] == "self":
                if int(arguments[1]) > 0:
                    self.descriptor.character.current_hp -= int(
                        arguments[1])
            elif arguments[0] == "bandyta":
                if int(arguments[1]) > 0:
                    self.enemies[0].current_hp -= int(
                        arguments[1])


class Descriptor:
    def __init__(self, reader, writer):
        self.reader = reader
        self.writer = writer
        self.socket = writer.get_extra_info("socket")
        self.peer = self.socket.getpeername()
        self.logger = logging.getLogger("%s:%d" % self.peer)
        self.encoding = DEFAULT_ENCODING
        self.character = None
        self.game_state = None

    def send(self, message):
        encoded = ("%s\n" % message).encode(self.encoding)
        self.writer.write(encoded)
        self.logger.debug("Send: %s" % message)

    async def read(self) -> str:
        data = await self.reader.readline()
        decoded = data.decode(self.encoding).strip()
        self.logger.debug("Receive: %s" % decoded)
        return decoded

    async def disconnect(self):
        await self.writer.drain()
        self.writer.close()

    def is_connected(self):
        return not self.writer.is_closing()

    def set_game_state(self, game_state, *args):
        self.game_state = game_state(self, *args)
        self.game_state.initialize()


async def connection(reader, writer):
    """
    :param asyncio.StreamReader reader:
    :param asyncio.StreamWriter writer:
    :return None:
    """
    server_logger = logging.getLogger("server")
    descriptor = Descriptor(reader, writer)
    server_logger.debug("Received connection from %s at %d" %
                        descriptor.peer)
    descriptors[descriptor.peer] = descriptor
    descriptor.set_game_state(Authentication)

    while not writer.is_closing():
        try:
            if reader.at_eof():
                writer.close()
                break

            print(descriptor.game_state.__dict__)
            await descriptor.game_state.parse_input()
        except Exception as error:
            (exception_type,
             exception_value,
             exception_traceback) = sys.exc_info()
            print(exception_type)
            print(exception_value)
            descriptor.send("Unexpected exception occurred. "
                            "Closing connection.")
            await descriptor.disconnect()
            server_logger.error("Exception: %s" % error)
            server_logger.error("".join(
                traceback.extract_tb(exception_traceback).format()
            ).strip())
    server_logger.debug("Connection from %s at %d closed" %
                        descriptor.peer)
    if descriptor.peer in descriptors and not descriptor.is_connected():
        del descriptors[descriptor.peer]


async def main():
    server_logger = logging.getLogger("server")
    server_logger.debug("Starting server at %s:%d" % (
        SERVER_HOSTNAME,
        SERVER_PORT
    ))

    server = await asyncio.start_server(
        connection,
        SERVER_HOSTNAME,
        SERVER_PORT
    )

    async with server:
        await server.serve_forever()

    server_logger.debug("Server stopped")

if __name__ == "__main__":
    asyncio.run(main())
